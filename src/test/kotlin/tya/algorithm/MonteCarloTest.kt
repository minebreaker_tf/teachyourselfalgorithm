package tya.algorithm

import org.hamcrest.number.IsCloseTo.closeTo
import org.junit.Assert.assertThat
import org.junit.Test
import java.util.*

class MonteCarloTest {

    @Test
    fun test() {
        val pi = piEstimate(10000, Random(0L))
        assertThat(pi, closeTo(3.14, 0.1))
    }

}
