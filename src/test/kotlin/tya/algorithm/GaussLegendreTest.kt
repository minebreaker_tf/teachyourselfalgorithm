package tya.algorithm

import org.hamcrest.number.BigDecimalCloseTo.closeTo
import org.junit.Assert.assertThat
import org.junit.Test
import java.math.BigDecimal
import java.math.MathContext

class GaussLegendreTest {

    @Test
    fun testNewtonRaphson() {
        assertThat(BigDecimal(2).sqrt(), closeTo(BigDecimal("1.4142135"), BigDecimal(0.000001)))
        assertThat(BigDecimal(3).sqrt(), closeTo(BigDecimal("1.7320508"), BigDecimal(0.000001)))
        assertThat(BigDecimal(4).sqrt(), closeTo(BigDecimal("2.0000000"), BigDecimal(0.000001)))
        assertThat(BigDecimal(5).sqrt(), closeTo(BigDecimal("2.2360679"), BigDecimal(0.000001)))
    }

    @Test
    fun testGaussLegendre() {
        val expected = BigDecimal("3.1415926535897932382")

        assertThat(gaussLegendre(1, MathContext.DECIMAL32), closeTo(expected, BigDecimal(0.01)))
        assertThat(gaussLegendre(2, MathContext.DECIMAL64), closeTo(expected, BigDecimal(0.00000001)))
        assertThat(gaussLegendre(3, MathContext.DECIMAL128), closeTo(expected, BigDecimal(0.000000000000000001)))
    }

}
