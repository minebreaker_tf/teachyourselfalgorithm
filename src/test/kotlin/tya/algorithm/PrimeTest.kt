package tya.algorithm

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class PrimeTest {

    var primes = intArrayOf()

    @Before
    fun setUp() {
        primes = intArrayOf(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97)
    }

    fun testZeroToHundred(evaluator: (Int) -> Boolean) {
        for (i in 1..100) {
            val res = evaluator(i)
            if (primes.contains(i)) {
                assertTrue(res)
            } else {
                assertFalse(res)
            }
        }
    }

    @Test
    fun testLoop() {
        testZeroToHundred(::isPrimeLoop)
    }

    @Test
    fun testRecursive() {
        testZeroToHundred(::isPrimeRecursive)
    }

    @Test
    fun testOptimized() {
        testZeroToHundred(::isPrimeOptimized)
    }

    @Test
    fun testEratosthenes() {
        testZeroToHundred(::isPrimeEratosthenesSieve)
    }

}
