package tya.algorithm.cycle

import com.google.common.collect.Iterables
import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test

internal class FloydKtTest {

    @Test
    fun test() {
        val list = MutableLinkedList<Int>().cons(6).cons(5).cons(4).cons(3).cons(2).cons(1)
        list.first!!.next!!.next!!.next!!.next!!.next!!.next = list.first!!.next!!.next!!

        assertThat(Iterables.limit(list, 12)).containsExactly(1, 2, 3, 4, 5, 6, 3, 4, 5, 6, 3, 4)

        assertThat(isCycle(listOf(1, 2, 3))).isFalse()
        assertThat(isCycle(listOf<Int>())).isFalse()
        assertThat(isCycle(list)).isTrue()
    }
}
