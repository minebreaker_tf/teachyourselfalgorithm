package tya.algorithm.sort

import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Test
import java.util.*

class SortTest {

    @Test
    fun testIsSorted() {
        assertTrue(intArrayOf(1, 2, 3, 4).isSorted())
        assertTrue(intArrayOf(1, 2, 2, 3).isSorted())
        assertFalse(intArrayOf(4, 3, 2, 1).isSorted())
        assertFalse(intArrayOf(3, 2, 2, 1).isSorted())
        assertTrue(intArrayOf(1, 1, 1, 1).isSorted())
    }

    @Test
    fun testSwap() {
        assertThat(intArrayOf(1, 3, 2, 4).swap(1, 2), `is`(intArrayOf(1, 2, 3, 4)))
        assertThat(intArrayOf(2, 1, 3, 4).swap(0, 1), `is`(intArrayOf(1, 2, 3, 4)))
        assertThat(intArrayOf(1, 2, 4, 3).swap(2, 3), `is`(intArrayOf(1, 2, 3, 4)))
        assertThat(intArrayOf(4, 2, 3, 1).swap(3, 0), `is`(intArrayOf(1, 2, 3, 4)))
    }

    @Test
    fun testBogoSort() {
        val res = bogoSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), Random(1L), stdoutSortListener)
        assertTrue(res.isSorted())
    }

    @Test
    fun testBubbleSort() {
        val res = bubbleSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), stdoutSortListener)
        assertTrue(res.isSorted())
    }

    @Test
    fun testShakerSort() {
        val res = shakerSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), stdoutSortListener)
        assertTrue(res.isSorted())
    }

    @Test
    fun testCombSort() {
        val res = combSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), stdoutSortListener)
        assertTrue(res.isSorted())
    }

    @Test
    fun testGnomeSort() {
        val res = gnomeSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), stdoutSortListener)
        assertTrue(res.isSorted())
    }

    @Test
    fun testMergeSort() {
        val res = mergeSort(intArrayOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1), stdoutSortListener)
        assertTrue(res.isSorted())
    }

}
