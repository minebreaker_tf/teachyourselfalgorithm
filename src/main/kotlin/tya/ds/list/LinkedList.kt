package tya.ds.list

class LinkedList<in E> {

    private var last: Node<E>? = null
    private val root: Node<E> = Node(null, last)

    fun add(element: E) {
        if (last == null) {
            last = Node(element, null)
        } else {
            last!!.next = Node(element, null)
            last = last!!.next
        }
    }

}

class Node<E>(var element: E?, var next: Node<E>?)

fun <E> reverse(head: Node<E>): Node<E> {

    fun <E> reverseUntil(curr: Node<E>, prev: Node<E>?): Node<E> {

        val next = curr.next

        if (next == null) {
            curr.next = prev
            return curr
        }

        curr.next = prev

        return reverseUntil(next, curr)
    }

    return reverseUntil(head, null)

}

fun main(args: Array<String>) {
    val head = Node("1", Node("2", Node("3", Node("4", null))))

    val reversed = reverse(head)
    print(reversed.element)
    print(reversed.next!!.element)
    print(reversed.next!!.next!!.element)
    print(reversed.next!!.next!!.next!!.element)
}
