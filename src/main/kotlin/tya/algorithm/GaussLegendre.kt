package tya.algorithm

import java.math.BigDecimal
import java.math.MathContext

val TWO = BigDecimal(2)
val FOUR = BigDecimal(4)
val ONE_FORTH = BigDecimal("0.25")

fun BigDecimal.sqrt(context: MathContext = MathContext.DECIMAL32): BigDecimal {
    return newtonRaphson(BigDecimal(Math.sqrt(this.toDouble())), context, this)
}

infix fun BigDecimal.same(other: BigDecimal): Boolean {
    return this.compareTo(other) == 0
}

/**
 * Calculates sqrt of the given number using Newton-Raphson's method.
 *
 * `x_n+1 = 1/2 ( x_n + 2 / x_n )`
 *
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%8B%E3%83%A5%E3%83%BC%E3%83%88%E3%83%B3%E6%B3%95)
 * [Stack Overflow](http://stackoverflow.com/questions/13649703/square-root-of-bigdecimal-in-java)
 */
fun newtonRaphson(x: BigDecimal, context: MathContext = MathContext.DECIMAL32, a: BigDecimal = x): BigDecimal {
    val x1 = (x.add(a.divide(x, context))).divide(TWO, context)

    // repeat recursively until it produces proper precision
    if (x same x1) {
        return x1
    }

    return newtonRaphson(x1, context, a)
}

/**
 * Estimates π using Gauss-Legendre algorithm.
 *
 * `π ≒ (a + b)^2 / 4t`
 *
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%AC%E3%82%A6%E3%82%B9%EF%BC%9D%E3%83%AB%E3%82%B8%E3%83%A3%E3%83%B3%E3%83%89%E3%83%AB%E3%81%AE%E3%82%A2%E3%83%AB%E3%82%B4%E3%83%AA%E3%82%BA%E3%83%A0)
 *
 * TODO: memoize
 * TODO: auto-determine n
 *
 * @param n Times to recurse
 * @param context Context used
 * @return Estimated pi
 */
fun gaussLegendre(n: Int, context: MathContext): BigDecimal {
    return (a(n, context) + (b(n, context))).pow(2).divide(FOUR * (t(n, context)), context)
}

/**
 * `a_0 = 1`
 * `a_n+1 = ( a_n + b_n ) / 2`
 */
private fun a(n: Int, context: MathContext): BigDecimal {
    return if (n == 0) BigDecimal.ONE
    else (a(n - 1, context) + (b(n - 1, context))).divide(TWO, context)
}

/**
 * `b_0 = 1 / sqrt(2)`
 * `b_n+1 = sqrt( a_n b_n )`
 */
private fun b(n: Int, context: MathContext): BigDecimal {
    return if (n == 0) BigDecimal.ONE.divide(TWO.sqrt(context), context)
    else (a(n - 1, context) * (b(n - 1, context))).sqrt(context)
}

/**
 * `t_0 = 1 / 4`
 * `t_n+1 = t_n - p_n ( a_n - a_n+1 )^2`
 */
private fun t(n: Int, context: MathContext): BigDecimal {
    return if (n == 0) ONE_FORTH
    else t(n - 1, context) - (p(n - 1, context) * ((a(n - 1, context) - (a(n, context))).pow(2)))
}

/**
 * `p_0 = 1`
 * `p_n+1 = 2 p_n`
 */
private fun p(n: Int, context: MathContext): BigDecimal {
    return if (n == 0) BigDecimal.ONE
    else TWO * (p(n - 1, context))
}

fun main(args: Array<String>) {
    println(BigDecimal(2).sqrt(MathContext.DECIMAL32))
    println(BigDecimal(3).sqrt(MathContext.DECIMAL32))
    println(BigDecimal(4).sqrt(MathContext.DECIMAL32))
    println(BigDecimal(5).sqrt(MathContext.DECIMAL32))
    println()
    println(gaussLegendre(1, MathContext.DECIMAL128))
    println(gaussLegendre(2, MathContext.DECIMAL128))
    println(gaussLegendre(3, MathContext.DECIMAL128))
}
