package tya.algorithm

/**
 * Returns if the given number is a prime or not, using for loop.
 */
fun isPrimeLoop(number: Int): Boolean {

    if (number <= 1) {
        return false
    }

    @Suppress("LoopToCallChain")
    for (i in 2 until number) {
        if (number.rem(i) == 0) {
            return false
        }
    }
    return true

}

/**
 * Implementation using recursion.
 */
fun isPrimeRecursive(number: Int): Boolean {

    if (number <= 1) {
        return false
    }

    fun dividableByEach(number: Int, by: Int): Boolean {

        if (by <= 1) {
            return true
        }

        if (number.rem(by) == 0) {
            return false
        }

        return dividableByEach(number, by - 1)
    }

    return dividableByEach(number, number - 1)

}

/**
 * Recursive implementation with some optimization.
 */
fun isPrimeOptimized(number: Int): Boolean {

    if (number <= 1) {
        return false
    }

    // dividable by 2 (and not 2 itself)
    if (number and 1 == 0 && number != 2) {
        return false
    }

    val limit = Math.sqrt(number.toDouble()).toInt()

    fun dividableByEach(number: Int, by: Int): Boolean {

        if (by > limit) {
            return true
        }

        if (number.rem(by) == 0) {
            return false
        }

        // skip even numbers
        return dividableByEach(number, by + 2)
    }

    return dividableByEach(number, 3)

}

var table = eratosthenesSieve(1000)

/**
 * Creates Sieve of Eratosthenes.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%A8%E3%83%A9%E3%83%88%E3%82%B9%E3%83%86%E3%83%8D%E3%82%B9%E3%81%AE%E7%AF%A9)
 */
fun eratosthenesSieve(volume: Int): Array<Boolean> {

    val table = Array(volume, init = { true })
    val limit = Math.sqrt(volume.toDouble()).toInt()

    table[0] = false // 1 is prime

    for (i in 2..limit) {
        if (table[i - 1]) {
            for (j in i * 2..volume step i) {
                table[j - 1] = false
            }
        }
    }

    return table
}

/**
 * Implementation using Sieve of Eratosthenes.
 */
fun isPrimeEratosthenesSieve(number: Int): Boolean {

    if (number >= table.size) {
        table = eratosthenesSieve(number * 2)
    }

    if (number <= 0) {
        return false
    }

    return table[number - 1]
}

fun main(args: Array<String>) {
    for (i in 1..100) {
        println("" + i + ": " + isPrimeLoop(i))
        println("" + i + ": " + isPrimeRecursive(i))
        println("" + i + ": " + isPrimeOptimized(i))
        println("" + i + ": " + isPrimeEratosthenesSieve(i))
    }
}
