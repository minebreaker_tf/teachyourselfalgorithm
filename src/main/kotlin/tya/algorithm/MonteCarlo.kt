package tya.algorithm

import java.util.*

/**
 * Estimates π using Monte-Carlo algorithm.
 *
 * [Wikipedia jp](https://ja.wikipedia.org/wiki/%E3%83%A2%E3%83%B3%E3%83%86%E3%82%AB%E3%83%AB%E3%83%AD%E6%B3%95)
 * [Wikipedia en](https://en.wikipedia.org/wiki/Monte_Carlo_method)
 *
 * @param accuracy Times to try. Default is 10000.
 * @param randomGenerator [Random] instance to create random value.
 * @return Estimated Pi value
 */
fun piEstimate(accuracy: Int = 10000, randomGenerator: Random = Random()): Double {

    // Times to try
    val n = accuracy

    var p = 0
    for (i in 0 until n) {
        val x = randomGenerator.nextDouble()
        val y = randomGenerator.nextDouble()

        // count points in the unit circle
        if (x * x + y * y <= 1) {
            p += 1
        }
    }

    //.p/n = [Area of the circle]
    //     = π/4
    return (p * 4).toDouble() / n

}

fun main(args: Array<String>) {
    println(piEstimate())
}
