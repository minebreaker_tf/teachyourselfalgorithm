package tya.algorithm.cycle

data class Node<T>(val value: T, var next: Node<T>?)

class MutableLinkedList<T> : Iterable<T> {

    var first: Node<T>? = null

    fun cons(newValue: T): MutableLinkedList<T> {
        val newNode = Node(newValue, first)
        first = newNode
        return this
    }

    override fun iterator() = object : Iterator<T> {
        var current: Node<T>? = first
        override fun hasNext() = current != null
        override fun next(): T {
            if (current == null) throw NoSuchElementException()
            val value = current!!.value
            current = current!!.next
            return value
        }
    }
}

fun isCycle(iterable: Iterable<*>) = isCycle(iterable.iterator(), iterable.iterator())

fun isCycle(fastIterator: Iterator<*>, slowIterator: Iterator<*>): Boolean {
    if (!fastIterator.hasNext()) return false
    fastIterator.next()
    if (!fastIterator.hasNext()) return false
    if (fastIterator.next() == slowIterator.next()) return true
    return isCycle(fastIterator, slowIterator)
}