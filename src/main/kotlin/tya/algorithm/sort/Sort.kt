package tya.algorithm.sort

import java.util.*

fun IntArray.isSorted(): Boolean {
    val arr = this

    for (i in 1 until arr.size) {
        if (arr[i - 1] > arr[i]) {
            return false
        }
    }

    return true
}

fun IntArray.swap(i: Int, j: Int): IntArray {
    val temp = this[i]
    this[i] = this[j]
    this[j] = temp

    return this
}

/**
 * BogoSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%9C%E3%82%B4%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun bogoSort(arr: IntArray, rnd: Random = Random(), listener: SortListener = doNothingSortListener): IntArray {
    listener.display(arr)

    val len = arr.size
    while (!arr.isSorted()) {
        for (i in 1 until len) {
            arr.swap(len - i, rnd.nextInt(len - i))
        }

        listener.display(arr)
    }

    return arr
}

/**
 * BubbleSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%90%E3%83%96%E3%83%AB%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun bubbleSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {
    listener.display(arr)

    val len = arr.size
    for (i in 0 until len) {
        for (j in 0 until len - i - 1) {
            if (arr[j] > arr[j + 1]) {
                arr.swap(j, j + 1)
                listener.display(arr)
            }
        }
    }

    return arr
}

/**
 * ShakerSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B7%E3%82%A7%E3%83%BC%E3%82%AB%E3%83%BC%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun shakerSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {
    listener.display(arr)

    val len = arr.size

    while (true) {
        var start = 0
        var end = len - 1

        var lastSwap = 0

        for (i in start until end) {
            if (arr[i] > arr[i + 1]) {
                arr.swap(i, i + 1)
                listener.display(arr)
                lastSwap = i
            }
        }
        end = lastSwap

        if (start == end) break

        // for (i = end; i > start ; i--) {
        for (j in start until end) {
            val i = start + end - j

            if (arr[i] < arr[i - 1]) {
                arr.swap(i, i - 1)
                listener.display(arr)
                lastSwap = i
            }
        }

        start = lastSwap

        if (start == end) break

    }

    return arr
}

/**
 * CombSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%A0%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun combSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {
    listener.display(arr)

    val len = arr.size
    var h: Int = len
    var swaped = false

    while (h > 1 || swaped) {
        swaped = false

        if (h > 1) {
            h = h * 10 / 13
        }

        var i = 0
        while (i + h < len) {
            if (arr[i] > arr[i + h]) {
                arr.swap(i, i + h)
                listener.display(arr)
                swaped = true
            }
            i++
        }
    }

    return arr
}

/**
 * GnomeSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%8E%E3%83%BC%E3%83%A0%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun gnomeSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {

    val len = arr.size
    var i = 0
    while (i < len - 1) {
        if (arr[i] <= arr[i + 1]) {
            i++
        } else {
            arr.swap(i, i + 1)
            listener.display(arr)
            if (i > 0) {
                i--
            }
        }
    }

    return arr
}

fun selectSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {

    for (i in 0 until arr.size - 1) {

    }

    TODO()
}

/**
 * MergeSort.
 * [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%BC%E3%82%B8%E3%82%BD%E3%83%BC%E3%83%88)
 */
fun mergeSort(arr: IntArray, listener: SortListener = doNothingSortListener): IntArray {

    fun merge(arr1: IntArray, arr2: IntArray): IntArray {
        val len1 = arr1.size
        val len2 = arr2.size

        val res = IntArray(len1 + len2)
        var a = arr1[0]
        var b = arr2[0]
        var i = 0
        var j = 0
        var k = 0

        while (true) {
            if (arr1[j] <= arr2[k]) {
                res[i] = a
                i += 1
                j += 1
                if (j >= len1) break
                a = arr1[j]
            } else {
                res[i] = b
                listener.display(res)
                i += 1
                k += 1
                if (k >= len2) break
                b = arr2[k]
            }
        }
        while (j < len1) {
            res[i] = arr1[j]
            listener.display(res)
            i += 1
            j += 1
        }
        while (k < len2) {
            res[i] = arr2[k]
            listener.display(res)
            i += 1
            k += 1
        }
        return res
    }

    if (arr.size <= 1) return arr

    val split = arr.size shr 1
    val arr1 = Arrays.copyOfRange(arr, 0, split)
    val arr2 = Arrays.copyOfRange(arr, split, arr.size)

    return merge(mergeSort(arr1), mergeSort(arr2))
}
