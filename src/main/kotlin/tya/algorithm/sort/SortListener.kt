package tya.algorithm.sort

import java.util.*

interface SortListener {
    fun display(arr: IntArray)
}

val doNothingSortListener = object : SortListener {
    override fun display(arr: IntArray) {
        // do nothing
    }
}

val stdoutSortListener = object : SortListener {
    override fun display(arr: IntArray) {
        println(Arrays.toString(arr))
    }
}
